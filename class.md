### Classes[12] = "Reduced Representation Libraries"

#### Análise de Sequências Biológicas 2023

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->What are RRLs?
* &shy;<!-- .element: class="fragment" -->RRL types
 * &shy;<!-- .element: class="fragment" -->What are RRLs useful for?
 * &shy;<!-- .element: class="fragment" -->What are RRLs **not** useful for?
* &shy;<!-- .element: class="fragment" -->RRL data analysis

---

### What are RRLs?

* &shy;<!-- .element: class="fragment" -->A form of genomic *fingerprinting*
  * &shy;<!-- .element: class="fragment" -->Resort to one or more [restriction enzymes](https://biologydictionary.net/restriction-enzymes/) to "chop" the genome
![Restriction enzymes on a DNA fragment](assets/fingerprinting.jpg)
  * &shy;<!-- .element: class="fragment" -->*Reads* are produced from a limited number of genomic regions
  * &shy;<!-- .element: class="fragment" -->*Reads* are tagged
* &shy;<!-- .element: class="fragment" -->An economic form of obtaining large batches of genomic variation


|||

### The concept

1. &shy;<!-- .element: class="fragment" -->Reduce the genome
2. &shy;<!-- .element: class="fragment" -->Sequence target regions
3. &shy;<!-- .element: class="fragment" -->Assign sequences to samples
4. &shy;<!-- .element: class="fragment" -->Stack same region data
5. &shy;<!-- .element: class="fragment" -->Call variants between samples

|||

### How it works

&shy;<!-- .element: class="fragment" -->[![A RAD-Seq diagram](assets/floragenex-rad-dna-sequencing-bioinformatics-rad-diagram_small.png)](assets/floragenex-rad-dna-sequencing-bioinformatics-rad-diagram.png)

---

### Most common RRL types

* &shy;<!-- .element: class="fragment" -->RAD-Seq (**R**estriction site **A**ssociated **D**NA)
 * &shy;<!-- .element: class="fragment" -->Same length barcodes
 * &shy;<!-- .element: class="fragment" -->Max 48 individuals per run
 * &shy;<!-- .element: class="fragment" -->Maximizes reads per sample
* &shy;<!-- .element: class="fragment" -->GBS (**G**enotyping **b**y **S**equencing)
 * &shy;<!-- .element: class="fragment" -->Variable length barcodes
 * &shy;<!-- .element: class="fragment" -->Max 96 individuals per run
 * &shy;<!-- .element: class="fragment" -->Maximizes individuals per run
* &shy;<!-- .element: class="fragment" -->These lines are nowadays *very* blurred 

---

### RRL usage

* &shy;<!-- .element: class="fragment" -->[Progenies](http://www.biomedcentral.com/1471-2164/14/566/abstract)
* &shy;<!-- .element: class="fragment" -->[Linkage maps construction](http://www.biomedcentral.com/1471-2164/15/166)
* &shy;<!-- .element: class="fragment" -->[Population Genomics](http://onlinelibrary.wiley.com/doi/10.1111/tpj.12370/abstract)
* &shy;<!-- .element: class="fragment" -->[Demography](https://doi.org/10.1038/s41437-017-0037-y)
* &shy;<!-- .element: class="fragment" -->[Association studies](https://doi.org/10.1111/gcb.14497)

---

### RRL non-usage

* &shy;<!-- .element: class="fragment" -->Genome assembly

&shy;<!-- .element: class="fragment" -->![A man pulling a donkey in a cart](assets/wrong1.jpg)

|||

### RRL non-usage

* &shy;<!-- .element: class="fragment" -->Gene expression analysis

&shy;<!-- .element: class="fragment" -->![A girl "flying" out of a swing](assets/wrong2.jpg)

|||

### RRL non-usage


* &shy;<!-- .element: class="fragment" -->Gene discovery


&shy;<!-- .element: class="fragment" -->![A soldier using a rifle like a longbow](assets/wrong3.jpg)

---

### In practice

1. &shy;<!-- .element: class="fragment" -->Obtain data
2. &shy;<!-- .element: class="fragment" -->*Read demultiplexing*

&shy;<!-- .element: class="fragment" -->![A scheme of samples being identified by barcodes](assets/demultiplex.png)

|||

### In practice

3. &shy;<!-- .element: class="fragment" -->*Loci* clustering per sample
4. &shy;<!-- .element: class="fragment" -->*Loci* clustering multisample
5. &shy;<!-- .element: class="fragment" -->Finding variants

&shy;<!-- .element: class="fragment" -->![DNA sequence "stacks"](assets/clustering.png)

|||

### In practice

6. &shy;<!-- .element: class="fragment" -->Biological analyses

&shy;<!-- .element: class="fragment" -->![Quercus suber population structure](assets/Qsuber_structure.png)

---

### Your task

* Learn to assemble Reduced Representation Libraries data
* Use [this tutorial](../../assignments/Assignment01.5.html), which was built for the effect
 * Uses simulated data
 * Very fast
* Obtain a VCF file
* Obtain a Phylip file

**Mastering these skills will be crucial for your next Assignment!**

---

### References

* [Palaiokostas et al., 2013](http://www.biomedcentral.com/1471-2164/14/566)
* [Gonen et al., 2014](http://www.biomedcentral.com/1471-2164/15/166)
* [Xu et al., 2013](http://onlinelibrary.wiley.com/doi/10.1111/tpj.12370/abstract)
* [Nunziata & Weisrock, 2018](https://doi.org/10.1038/s41437-017-0037-y)
* [Pina-Martins et al., 2019](https://doi.org/10.1111/gcb.14497)
* [RADCamp Lisbon 2020](https://radcamp.github.io/Lisbon2020/)

